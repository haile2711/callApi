import { GET_LIST_DOCUMENT, SAVE_LIST_DOCUMENT } from './types';

export const getListDocument = (...args: any[]) => ({
    type: GET_LIST_DOCUMENT,
    args,
});

export const saveListDocument = (payload: any) => ({
    type: SAVE_LIST_DOCUMENT,
    payload,
});
