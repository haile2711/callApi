import { GET_LIST_USER, SAVE_LIST_USER } from './types';

export const actGetListUser = (...args: any[]) => ({
    type: GET_LIST_USER,
    args,
});

export const actSaveListUser = (payload: any) => ({
    type: SAVE_LIST_USER,
    payload,
});
