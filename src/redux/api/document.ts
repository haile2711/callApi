import { API } from './common';

export default {
    getListDocument: (params: string) => API.get(`document/list?${params}`),
};
