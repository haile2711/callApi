import demo from './demo';
import auth from './auth';
import user from './user';

export default {
    ...demo,
    ...auth,
    ...user,
};
