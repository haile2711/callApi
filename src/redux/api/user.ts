import { API } from './common';
// page=1&perPage=10&orderBy=createdAt&order=DESC
export default {
    getListUser: (params: string) => API.get(`/user/list/guest?${params}`),
};
