import { SAVE_LIST_DOCUMENT } from '@redux/actions/types';

const initialState = {
    documents: [],
};

const document = (state = initialState, { type, payload }: any) => {
    switch (type) {
        case SAVE_LIST_DOCUMENT:
            return {
                ...state,
                documents: [...payload],
            };
        default:
            return state;
    }
};

export default document;
