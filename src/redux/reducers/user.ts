import { SAVE_LIST_USER } from '@redux/actions/types';

const initialState = {
    users: [],
};

const user = (state = initialState, { type, payload }: any) => {
    switch (type) {
        case SAVE_LIST_USER:
            return {
                ...state,
                users: [...payload],
            };
        default:
            return state;
    }
};

export default user;
