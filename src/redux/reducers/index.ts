import { combineReducers } from 'redux';

import requests from './common';
import auth from './auth';
import user from './user';
import document from './document';

export default combineReducers({
    requests,
    auth,
    user,
    document,
});
