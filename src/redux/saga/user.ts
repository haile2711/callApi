import logger from '@libs/logger';
import { actSaveListUser } from '@redux/actions';
import { GET_LIST_USER } from '@redux/actions/types';
import user from '@redux/api';
import { all, takeLatest } from 'redux-saga/effects';

import { createRequestSaga } from './common';

const requestGetListUser = createRequestSaga({
    key: 'getListUser',
    request: user.getListUser,
    success: [(res: any) => actSaveListUser(res.data)],
    failure: [(res: any) => actSaveListUser(res)],
    // functionSuccess: [(res: any) => logger.debug('users', res)],
});

export default [
    function* fetchWatcher() {
        yield all([takeLatest(GET_LIST_USER, requestGetListUser)]);
    },
];
