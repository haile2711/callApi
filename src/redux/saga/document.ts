import { saveListDocument } from '@redux/actions';
import { GET_LIST_DOCUMENT } from '@redux/actions/types';
import document from '@redux/api/document';
import { all, takeLatest } from 'redux-saga/effects';

import { createRequestSaga } from './common';

const requestGetListDocument = createRequestSaga({
    key: 'getListDocument',
    request: document.getListDocument,
    success: [(res: any) => saveListDocument(res.data)],
    failure: [(res: any) => saveListDocument(res)],
    // functionSuccess: [(res: any) => console.log('documents', res)],
});
export default [
    function* fetchWatcher() {
        yield all([takeLatest(GET_LIST_DOCUMENT, requestGetListDocument)]);
    },
];
