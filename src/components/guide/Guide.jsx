/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/button-has-type */

import { useSelector } from 'react-redux';

export default function Guide() {
    const guide = useSelector((state) => state.document.documents);
    const limitGuide = guide.slice(guide.length - 4);
    console.log(limitGuide);
    return (
        <div className='guide'>
            <div className='wrapper'>
                <div className='top'>
                    <div className='titleTop'>
                        <div className='circle' />
                        <span className='text'>Guide: How to find your Funds</span>
                        <button className='btnShow'>Show all</button>
                    </div>
                    <div className='desc'>
                        Vietnam’s first innovation platform empowered by credit utility and more than 10K experts
                        network at
                    </div>
                </div>
                <div className='bottom'>
                    {limitGuide.map((guides) => (
                        <div className='container'>
                            <div className='imgGuide'>
                                <img src={guides.thumbnail} alt='' />
                            </div>
                            <div className='content'>
                                <div className='title'>{guides.title}</div>
                                <div className='date'>
                                    <img src='./assets/date.png' alt='' />
                                    <span>{new Date(guides.createdAt).toDateString()}</span>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}
