/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/button-has-type */

import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination, Navigation, Autoplay } from 'swiper';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getListDocument } from '@redux/actions';
import { convertObjToQuery } from '@libs/common';

export default function Documents() {
    const [documents, setDocuments] = useState([]);
    const dispatch = useDispatch();
    useEffect(() => {
        const params = {
            page: 1,
            perPage: 10,
        };
        dispatch(
            getListDocument(convertObjToQuery(params), (_: any, res: any) => {
                setDocuments(res.data);
            })
        );
    }, []);
    console.log(documents);
    return (
        <div className='documents'>
            <div className='wrapper'>
                <div className='top'>
                    <div className='titleTop'>
                        <div className='circle' />
                        <span className='text'>Documents: Contracts for Startups</span>
                        <button className='btnShow'>Show all</button>
                    </div>
                    <div className='desc'>Documents: Contracts for Startups</div>
                </div>
                <div className='bottom'>
                    <Swiper
                        slidesPerView={5}
                        slidesPerGroup={5}
                        loop
                        loopFillGroupWithBlank
                        pagination={{
                            clickable: true,
                        }}
                        autoplay={{
                            delay: 2500,
                        }}
                        navigation
                        modules={[Pagination, Navigation, Autoplay]}
                        className='mySwiper'
                    >
                        {documents.map((doc) => (
                            <SwiperSlide>
                                <div className='container'>
                                    <img className='imgDocument' src={doc?.thumbnail} alt='' />
                                    <div className='title'>{doc?.title}</div>
                                    <div className='items'>
                                        <img src='./assets/heart.png' alt='' />
                                        <button>Download</button>
                                    </div>
                                </div>
                            </SwiperSlide>
                        ))}
                    </Swiper>
                </div>
            </div>
        </div>
    );
}
