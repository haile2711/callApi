import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import dynamic from 'next/dynamic';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actGetListUser } from '@redux/actions';
import { convertObjToQuery } from '@libs/common';

import Topbar from '../src/components/topbar/Topbar';
import Intro from '../src/components/intro/Intro';
import Card from '../src/components/card/Card';
import PostCard from '../src/components/postCard/PostCard';
import Talent from '../src/components/talents/Talent';
import Project from '../src/components/project/Project';
import Guide from '../src/components/guide/Guide';
import Documents from '../src/components/documents/Documents';
import Contact from '../src/components/contact/Contact';
import styles from '../styles/Home.module.scss';

const MainLayout = dynamic(() => import('@components/Layout'));

const Home = () => {
    const dispatch = useDispatch();
    const users = useSelector((state: any) => state.user.users);
    const [user, setUser] = useState([]);
    console.log('ln24', users);
    useEffect(() => {
        const params = {
            page: 1,
            perPage: 10,
        };

        dispatch(
            actGetListUser(convertObjToQuery(params), (_: any, res: any) => {
                setUser(res.data);
            })
        );
    }, []);
    console.log('ln38', user);
    return (
        <MainLayout title='Home' description='Home description' className={styles.container}>
            Homepage
            <Topbar />
            <div className={styles.main} />
            <Intro />
            <Card />
            <PostCard />
            <Talent user={user} />
            {/* <Project /> */}
            <Guide />
            <Documents />
            <Contact />
        </MainLayout>
    );
};

export const getStaticProps = async ({ locale }: any) => ({
    props: {
        ...(await serverSideTranslations(locale, ['common'])),
    },
});

export default Home;
